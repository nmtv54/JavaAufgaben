import java.net.URL;
import java.io.InputStream;
import org.w3c.dom.*; // Document, Element, NodeList, Node
import javax.xml.parsers.*; // DocumentBuilderFactory, DocumentBuilder
import javax.xml.xpath.*; // XPath, XPathFactory, XPathConstants

URL url = new URL("http://api.steampowered.com/ISteamWebAPIUtil/GetSupportedAPIList/v0001/?format=xml");
Document doc;
try(InputStream is = url.openStream()) {
  DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
  doc = db.parse(is);
}

XPath path = XPathFactory.newInstance().newXPath();        // <1>
NodeList nodes = (NodeList) path.evaluate(
                                "/apilist/interfaces/interface/name",
                                doc.getDocumentElement(),
                                XPathConstants.NODESET);
for(int i = 0; i < nodes.getLength(); i++) {
  String name = nodes.item(i).getFirstChild().getNodeValue();
  printf("%s\n", name);
}