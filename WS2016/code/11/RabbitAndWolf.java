class Rabbit                     {
        int weight = 1;
        String name;
        Rabbit(String name) { this.name = name; }
        void eat() { weight += 1; }
        int getWeight() { return weight; }
        String getName() { return name; }
}
class Wolf                        {
        int weight = 1;
        String name;
        Wolf(String name) { this.name = name; }
        void eat(Rabbit r) { weight += r.getWeight(); }
        int getWeight() { return weight; }
        String getName() { return name; }
}
