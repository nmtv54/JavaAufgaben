/**
 * Represents email addresses that follow the convention 
 * 'firstname.secondname.lastname@domain.com' where names can only
 * contain the letters 'a' to 'z' and the domain can additionally contain 
 * the digits '0' to '9' and the character '-'. Domains and names can have
 * an arbitrary number of parts separated by dots.
 */
interface SimpleEMail {
  /**
   * Returns the local name of the email address (the part before the '@').
   *
   * The result is split up into the different parts of the name of the
   * person (the parts between the dots).
   */
  String[] getNames();
  /**
   * Returns the domain of the email address (the part after the '@')
   */
  String getDomain();
}