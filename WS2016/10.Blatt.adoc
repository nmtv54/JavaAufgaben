= 10. Woche: OO-Aufgaben
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V1.0, 2016-12-16
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/
:imagedir: ./images

// Topics: https://git.thm.de/dhzb87/oop/issues/????

== Zeit und Datum

=== Geburstagskalender

Schreiben Sie einen Geburtstagskalender. Verwenden Sie dazu eine Klasse
`BirthdayDiary`, die eine `Collection` als interne Datenstruktur zur
Verwaltung der Kalendereinträge verwendet. Die Kalendereinträge sollen
Objekte der Klasse `Birthday` sein, die die Klasse
`java.util.GregorianCalendar` verwendet.

Die Klasse `BirthdayDiary` soll mindestens folgende Methoden haben:

* `void addBirthday(Birthday birthday)` - ein neuer Geburtstagseintrag
wird eingefügt
* `boolean deleteBirthday(Birthday birthday)` - löscht einen Eintrag
* `String toString()` - liefert eine "schöne" Darstellung des gesamten
Kalenders

Die Klasse `Birthday` soll mindestens folgende Konstruktoren bzw. Methoden
haben:

* `Birthday(String birthday)`- der String `birthday` wird geparsed und in
ein Objekt von `java.util.GregorianCalendar`- gewandelt; dieses Objekt ist Teil der internen Datenstruktur
* `String getWeekday()` - gibt für ein `Birthday`-Objekt den Wochentag zurück,
an dem die Person geboren ist
* `String getWeekdayByYear(int year)` - die Methode liefert für das
übergebene Jahr den Wochentag, an dem der Geburstag stattfindet

== Kommandozeile und Standard-Input

Die folgenden Programme verarbeiten Daten aus der sogenannten Standardeingabe, stdin. In der Regel wird die Standardeingabe in der Kommandozeile per Umleitungsoperatoren mit Daten versorgt, z.B. über die Tastatur, aus einer Datei oder mit den Ausgaben eines vorgelagerten Programms. In den Aufgaben sehen Sie Beispiele, wie das unter Windows geht.

// https://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/redirection.mspx?mfr=true

Für das Einlesen der Standard-Eingabe importieren Sie `java.io.*` und nutzen Sie:
[source,java]
----
BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
----
Die Eingabe kann nun Zeile für Zeile über eine `while`-Schleife gelesen werden:
[source,java]
----
while((line = in.readLine()) != null) {
    // body
}
----

.Text ist nicht gleich Text
****
Sie wissen, dass Zahlentypen wie `int` oder `float` verschiedene Kodierungen zur internen Speicherung von Zahlenwerten verwenden. Ähnlich ist es beim Umgang mit textuellen Informationen. Oftmals wird ein Byte zur Speicherung eines Textzeichens verwendet. Aber welche Binärfolge entspricht dem Zeichen "a", welche den deutschen Umlauten wie "ä", "ö", "ü" usw.? Das wird über Kodierungstabellen und -formate geregelt. Ein sehr altes Kodierformat aus den Anfangstagen der Informatik ist der https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange[American Standard Code for Information Interchange] (ASCII), der angepasst ist auf amerikanische Schriftzeichen. Noch viel in Gebrauch sind die Kodierungen nach https://de.wikipedia.org/wiki/ISO_8859[ISO 8859], ebenfalls eine Kodierung, die mit 8 Bits arbeitet und mit ASCII kompatibel ist. Modern ist das ebenfalls mit ASCII verträgliche Unicode-Format https://de.wikipedia.org/wiki/UTF-8[UTF-8], eine Kodierung, die der Vielfalt der Zeichen in den verschiedensten Sprachen und Kulturen gerecht zu werden versucht.

Wenn Sie beim Einlesen oder Schreiben von Text die Kodierung nicht angeben, verwendet die JVM die Kodierung, die als Standard für das jeweilige Betriebssystem eingestellt ist. Auf einem Windows-Rechner wird Windows Codepage 1252 als Kodierung angenommen, auf einem Linuxrechner UTF-8 und auf einem Mac entsprechend MacRoman. Je nach voreingestellter Sprache kann das noch einmal mehr variieren. So lange Sie nur Zeichen aus dem ASCII-Zeichensatz verwenden, fällt das Problem meistens nicht auf, da zumindest die drei oben genannten Kodierungen diese Zeichen mit der gleichen Bitfolge darstellen. Bei einer Datei in UTF-16 ist allerdings auch diese Übereinstimmung nicht mehr vorhanden.

Es gibt leider keine sichere Möglichkeit, einer Textdatei anzusehen, welche Kodierung sie verwendet. Darum bieten professionelle Tools zum Bearbeiten von Textdateien dem Benutzer die Möglichkeit, die Kodierung manuell auszuwählen.

Gewöhnen Sie sich daher am Besten schon jetzt an, beim Lesen und Schreiben von Text (egal ob aus einer Datei oder aus `System.in`) immer die Kodierung mit anzugeben. Das kann über die Übergabe eines `Charset`-Objekts beim Konstruktoraufruf des jeweiligen Reader oder Writer geschehen oder beim Aufruf der JVM über den JVM-Parameter `file.encoding` wie im folgenden Beispiel:

----
java -Dfile.encoding=UTF8 MyClass < /some/utf8-encoded/file.txt
----
****

[WARNING]
====
Normalerweise sollte man jeden `InputStream` oder `OutputStream`, den man in Java erzeugt auch wieder mit der Methode `close()` schließen, um die verwendeten Systemressourcen freizugeben. Eine der wenigen Ausnahmen bietet der hier verwendete `InputStream` `System.in`. Wenn dieser geschlossen wird, können danach keine Informationen mehr von der Konsole eingelesen werden. Daher darf dieser `InputStream` ausnahmsweise bis zum Programmende offen bleiben.
====

=== Grep

Unter Unix gibt es ein kleines Programm für die Kommandozeile namens `grep`. An `grep` übergibt man einen reguläres Ausdruck als Argument, `grep` listet dann alle Zeilen aus der Standardeingabe (stdin) auf. Ein Beispiel, wobei das Programm `grep` seinen eigenen Quellcode durchsucht:

----
>java Grep "class.*" < Grep.java
class Grep {

>java Grep ".*if\s?\(.*" < Grep.java
                  if (p.matcher(line).matches()) {
                      if (args.length == 2 && args[1].equals("-c"))
          if (args.length == 2 && args[1].equals("-c"))
----

Mit der Option `-c` kann man auch nur Anzahl der passenden Zeilen zählen lassen:

----
>java Grep ".*if\s?\(.*" -c < Grep.java
3
----

Entwickeln Sie Ihre eigene Implementierung von `grep`. Die ganze Anwendungslogik soll vollständig in der Methode `main` untergebracht werden.

ifdef::solution[]
.Lösung
----
include::{sourcedir}/10/Grep.java[]
----
endif::solution[]

=== UPN-Rechner mit Eingabe via STDIN

Sie haben bereits das Rechnen in der https://de.wikipedia.org/wiki/Umgekehrte_polnische_Notation[umgekehrt polnischen Notation (UPN)] kennengelernt. Es gilt, einen UPN-Rechner zu programmieren, der Eingaben über den sogenannten Standard-Input (stdin) entgegennimmt.

.Beispielhafte Interaktionsvarianten unter Windows
----
C:\Users\Dominikus\oop\heute>java UPNin < calc.txt //<1>
9 1 9

C:\Users\Dominikus\oop\heute>echo 1 2 + 3 * dup 1 swap | java UPNin //<2>
9 1 9

C:\Users\Dominikus\oop\heute>java UPNin //<3>
1 2 + 3 * dup 1 swap
^Z
9 1 9
----
<1> Das Java-Programm bezieht seinen Input aus der Datei `calc.txt`
<2> Der Konsolentext wird per `echo` auf den Standard-Input gegeben und über das `|`-Symbol an das Java-Programm weitergereicht
<3> Nach dem Start wartet das Programm auf eine Eingabe; die kann an der Konsole erfolgen und schliesst mit "Steuerung + Z"

Die Implementierung soll

* einen Stapel mit der Instanzvariable `Stack<Integer>` umsetzen
* in der Methode `void eval(Stack<Integer> s, String word)` die einzelnen Wörter (Zahlen und Operatoren) interpretieren und den Stapel entsprechend verändern
* die folgenden Operatoren in `eval` umsetzen:
  ** `dup` (oberstes Element duplizieren)
  ** `swap` (obersten beiden Elemente vertauschen)
  ** `drop` (oberstes Element entfernen)
  ** `neg` (Vorzeichen des obersten Elements wechseln)
  ** `+` Addition
  ** `-` Subtraktion als Kombination von `neg` und `+`
  ** `*` Multiplikation
  ** `/` Division
  ** `%` Rest der Division (Modulo)
* Fehleingaben tolerant behandeln
* intern tolerant mit Exceptions umgehen

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/10/UPNin.java[]
----
endif::solution[]

=== Holozän-Kalender

Der gregorianische Kalender, bei dem wir heute das Jahr 2016 schreiben, ist zwar in der westlichen Welt am weitestend verbreitet, aber bei genauerer Betrachtung ist er eigentlich ein recht veraltetes Konstrukt.

Zum einen basiert er auf dem christlichen Glauben an die Geburt von Jesus Christus und lässt damit alle Kulturen außer Acht, für die dieser keine besondere Bedeutung hat. Nach dem hebräischen Kalender, hätten wir zum Beispiel das Jahr 5777 während im islamischen Kalender das Jahr 1438 wäre. Außerdem unterschlagen wir mit der Wahl des Jahres eins vor etwas über zweitausend Jahren einen Großteil der Geschichte der menschlichen Zivilisation.

Eine recht charmante Lösung für diese Probleme ist der https://www.youtube.com/watch?v=czgOWmtGVGs[Holozän-Kalender], der einfach zehntausend Jahre auf den gregorianischen Kalender addiert. Damit liegt das Jahr eins etwa am Beginn des Holozäns, der geologischen Ära, in der die Menschen begannen von reinen Jägern und Sammlern zu sesshaften Viehzüchtern und Ackerbauern zu werden.

Helfen Sie der Java-Welt etwas fortschrittlicher zu werden und implementieren sie eine Klasse `HoloceneCalendar`, die die Klasse `java.util.GregorianCalendar` erweitert. Die Klasse soll die gleichen Konstruktoren haben wie ihre Superklasse, soll aber natürlich das aktuelle Datum in der Darstellung des Holozän-Kalenders angeben. Fügen Sie dieser Klasse eine Main-Methode hinzu, die das aktuelle Datum und die Uhrzeit mit Hilfe eines Objekts vom Typ `HoloceneCalendar` auf der Konsole ausgibt.

==== Holozän-Uhr
Statt bei einem Aufruf Ihres Programms nur einmal die aktuelle Uhrzeit zu sehen, wäre es doch viel schöner, wenn die Ausgabe wie eine echte Uhr die Zeit hochzählen würde so lange bis der Benutzer die Ausführung mit der Tastenkombination `Strg+C` abbricht. Implementieren Sie dieses Verhalten.

[TIP]
====
Um solche dynamischen Veränderungen in einer Kommandozeile zu realisieren können Sie sich der Steuerzeichen des ASCII-Zeichensatzes bedienen. Das Zeichen `'\b'` stellt ein sogenanntes _Backspace_ dar, das den Schreibcursor wieder um eine Stelle nach links verrückt. Das funktioniert allerdings nur innerhalb der aktuellen Zeile. Darüber hinaus müssten sie Betriebssystem-spezifische Steuerzeichen an die Konsole senden. Verwenden Sie daher für solche Aufgaben nur `System.out.print` und nicht `System.out.println`.

Zeitverzögerungen lassen sich mit der statischen Methode `Thread.sleep` realisieren.

Das folgende Beispiel zeigt erst die Zeichen `abc` auf der Konsole und ersetzt Sie dann nach einer Sekunde durch die Zeichen `xyz`.

----
System.out.print("abc");
Thread.sleep(1000);
System.out.print("\b\b\b");
System.out.print("xyz");
----
====

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/10/HoloceneCalendar.java[]
----
endif::solution[]

=== Midi-Keyboard

Der https://en.wikipedia.org/wiki/MIDI[Midi]-Synthesizer der Java-API ist bietet einen sehr einfachen Weg, Musik aus einem Java-Programm zu erzeugen. Der folgende Beispielcode spielt die Note C~4~ mit einem Klavierton. Die Verwendeten Klassen finden Sie in dem Paket `javax.sound.midi`.

[source, java]
----
Synthesizer s = MidiSystem.getSynthesizer();
s.open();
MidiChannel[] channels = s.getChannels();
channels[0].noteOn(60, 60);
Thread.sleep(300);
channels[0].noteOff(60);
s.close();
----

Schreiben Sie ein Java-Programm, mit dem der Benutzer auf der Konsole Tastenfolgen auf der C-Dur-Tonleiter eingeben kann, die dann mit einem Druck auf die Enter-Taste abgespielt werden. Der Ton C~4~ soll dabei mit dem zeichen `'c'` dargestellt werden, D~4~ als `'d'`, usw.. Ein Leerzeichen `' '` soll eine Pause von der Länge eines Tons darstellen. Der folgende Aufruf würde dann z.B. die ersten Töne der Mario-Titelmelodie abspielen:

----
D:\temp>java MidiKeyboard
eee ce g

----

[TIP]
====
Es ist wahrscheinlich, dass Ihre Java-Installation keine _Soundbank_ mitliefert, die nötig ist um die MIDI-Töne abzuspielen. Sie können sich diese Datei http://www.oracle.com/technetwork/java/soundbanks-135798.html[von der Oracle-Webseite herunterladen]. Sie muss in den Ordner `lib/audio` ihres Java-Verzeichnisses kopiert und in `soundbank.gm` umbenannt werden.

Bei der ersten Ausführung von Midi-Code auf der JVM müssen unter Windows außerdem augenscheinlich einige Registry-Einträge geschrieben werden. Wenn Sie eine Fehlermeldung beim Ausführen des Beispielcodes erhalten, versuchen Sie die JVM auf einer Konsole mit Administratorrechten zu starten.
====

[WARNING]
====
Wenn Sie sich dieser Aufgabe im Praktikum widmen, seien Sie gnädig mit ihren Kommilitonen und reduzieren Sie die Lautstärke ihres Rechners oder verwenden Sie am Besten gleich Kopfhörer. ;)
====

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/10/MidiKeyboard.java[]
----
<1> Dieser statische initialisierer ist notwendig, weil es keinen (schönen) Weg gibt, eine `Map` samt Inhalt in einer Zeile anzulegen.
endif::solution[]



== Verständnisfragen

=== Try

Finden Sie heraus, welche der folgenden Aussagen wahr sind:

. Hinter dem Schlüsselwort `try` kann eine Zuweisung in runden Klammern stehen.
. Hinter dem Schlüsselwort `try` _muss_ eine Zuweisung in runden Klammern stehen.
. Ein `try`-Block muss mindestens einen zugehörigen `catch`-Block besitzen.
. Ein `try`-Block kann auch alleine ohne zugehörigen `catch`-Block stehen.
. Ein `try`-Block kann auch alleine ohne zugehörigen `catch`-Block stehen, wenn er stattdessen einen `finally`-Block hat oder es sich um ein try-with-resources handelt.
. Ein `catch`-Block kann immer nur einen Typ von Exceptions (und alle Untertypen) fangen.
. Ein `catch`-Block kann auch zwei oder mehr unterschiedliche Typen von Exceptions fangen, die keine Unter- oder Überklassen voneinander sind.
. Der Code in einem `finally`-Block wird auch dann noch ausgeführt, wenn im `try`-Block ein `return`-Statement ausgeführt wird.
. Ein `return`-Statement im `try`-Block überspringt einen `finally`-Block.
. Die Reihenfolge von mehreren `catch`-Blöcken ist egal.
. Die Reihenfolge von mehreren `catch`-Blöcken kann das Verhalten eines Programms beeinflussen.

ifdef::solution[]
.Lösung
. Wahr. Diese Variante nennt man try-with-resources.
. Falsch. Dies ist nur für ein try-with-resources nötig.
. Falsch. Es gibt Ausnahmen (siehe 5).
. Falsch. Das gilt nicht immer (sieh 5).
. Wahr. Das sind genau die besagten Ausnahmen.
. Falsch. Siehe 7.
. Wahr. Mit Java 7 gibt es diese Möglichkeit, die wie folgt aussieht:
+
[source, java]
----
try {
  ...
} catch (NumberFormatException | IOException e) {
  ...
}
----
. Wahr. Das `finally` gewinnt immer. Der `finally`-Block gibt per Definition die Garantie, dass der darin enthaltene Code ausgeführt wird, bevor der zugehörige `try`-Block (auf welche Art auch immer) verlassen wird.
. Falsch. Siehe oben.
. Falsch. Siehe unten.
. Wahr. Es wird immer der erste passende `catch`-Block ausgeführt. Das folgende Beispiel wäre ein Programmierfehler, weil der zweite `catch`-Block nie erreicht würde. Ändert man die Reihenfolge, würde der erste Block Exceptions vom Typ `NumberFOrmatException` fangen und der zweite alle anderen.
+
[source, java]
----
try {
  ...
} catch (Exception e) {
  ...
} catch (NumberFormatException nfe) {
  ...
}
----
endif::solution[]
