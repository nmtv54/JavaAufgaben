interface CannibalInterface {
  // <1>
  /**
   * Moves the boat across the river taking the given number
   * of missionaries and cannibals.
   * 
   * @param cannibals number of cannibals on the boat
   * @param missionaries number of missionaries on the boat
   *
   * @throws NomNomException if any missionary is eaten
   * @throws IllegalArgumentException if the move is illegal
   */
  void moveBoat(int cannibals, int missionaries) throws NomNomException;
}