
int[][] pascalTriangle(int level){
  int[][] res = new int [level][];

  for (int i = 0; i < level; i++){
    res[i] = new int[i + 1];
  }

  for(int n = 0; n < level; n++){
    for (int k = 0; k <= n; k++){
      if((n == k) || (k == 0)){
          res[n][k] = 1;
      }
    }
  }

  for(int n = 2; n < level; n++){
    for (int k = 1; k < n; k++){
      res[n][k] = res[n-1][k-1] + res[n-1][k];
    }
  }

  return res;
}
