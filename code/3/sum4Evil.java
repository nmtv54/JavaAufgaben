int sum4(int[] a) {
    int sum = 0;
    for (int i = 0; i < 4 * a.length; sum += a[i++ % a.length]);
    return sum;
}

