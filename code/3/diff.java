int[] diff(int[] ar) {
  int[] res = new int[ar.length-1];
  for(int i = 0; i < ar.length-1; i++) {
    res[i] = ar[i+1] - ar[i];
  }
  return res;
}