
int mulF(int faktor1, int faktor2){
  int res = 0;
  for (int i = 1; i <= faktor1; i++){
    res = res + faktor2;
  }
  return res;
}


int mulW(int faktor1, int faktor2){
  int res = 0;
  int i = 1;
  while(i <= faktor1){
    res = res + faktor2;
    i++;
  }
  return res;
}


int mulDW(int faktor1, int faktor2){
  int res = 0;
  int i = 1;
  do{
    res = res + faktor2;
    i++;
  } while(i <= faktor1);

  return res;
}
