boolean sorted(int[] ar) {
  for(int i = 0; i < ar.length-1; i++) {
    if(ar[i] > ar[i+1]) return false;
  }
  return true;
}