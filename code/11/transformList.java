double[] rand(int n) {
  double[] ar = new double[n];
  for(int i = 0; i < ar.length; i++) {
    ar[i] = Math.random();
  }
  return ar;
}

void swap(int[] ar, int i1, int i2) {
  int tmp = ar[i1];
  ar[i1] = ar[i2];
  ar[i2] = tmp;
}

int[] keepPositives(int[] ar) {
  int[] arp = new int[ar.length];
  int count = 0;  
  for(int x : ar) {
    if(x >= 0) {
      arp[count] = x;
      count++;
    }
  }
  // trim to correct length
  int[] res = new int[count];
  for(int i = 0; i < count; i++) res[i] = arp[i];
  return res;
}
