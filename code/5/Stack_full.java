import java.util.Random;

class Stakk {
  int value;
  Stakk rest;

  Stakk(int value, Stakk rest) {
    this.value = value;
    this.rest = rest;
  }

  Stakk() { this(0, null); } 

  Stakk(int value) { this(value, new Stakk()); }
  
  public String toString() {
    if (rest == null) return "-|";
    return value + ", " + rest.toString();
  }
  
  int size() {
    if (rest == null) return 0;
    return 1 + rest.size();
  }
  
  void push(int val) {
    rest = new Stakk(value, rest);
    value = val;
  }
  
  int pop() {
    if (rest == null) throw new IllegalArgumentException("Stack underflow");
    int top = value;
    value = rest.value;
    rest  = rest.rest;
    return top;
  }
  
  int[] toArray() {
    int[] a = new int[size()];
    int i = 0;
    for(Stakk s = this; s.rest != null; s = s.rest)
      a[i++] = s.value;
    return a;
  }

  static Stakk fromArray(int[] a) {
    Stakk s = new Stakk();
    for(int i = a.length - 1; i >= 0; i--)
      s.push(a[i]);
    return s;
  }

  Stakk shuffle() {
    int[] a = this.toArray();
    Random r = new Random();
    for(int i = 0; i < a.length; i++) {
      int j = r.nextInt(a.length);
      int tmp = a[j];
      a[j] = a[i];
      a[i] = tmp;
    }
    return Stakk.fromArray(a);
  }
}