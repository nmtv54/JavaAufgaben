= Klassen
Dominikus Herzberg, Christopher Schölzel, Nadja Krümmel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:source-highlighter: coderay
:sourcedir: code/5

include::prelude.adoc[]

Die Aufgaben in diesem Kapitel üben das Programmieren mit Klassen ein.

.Auf der Suche nach Aufgaben ohne Mathe
****
Manche der Aufgaben beziehen Wissen aus der Schulmathematik ein. Zum Beispiel erwartet die Aufgabe zur Programmierung eines Bruchrechners, dass Sie die Bruchrechnung beherrschen. Leider (und zu unserem Bedauern) müssen wir vermehrt feststellen, dass Sie solche Grundlagen zum Teil nicht mitbringen -- was solche Aufgaben für Sie kaum lösbar macht. 

Darum brauchen wir Ihre Hilfe: Haben Sie Ideen oder Anregungen für Aufgaben, die ohne Mathe auskommen? Vielleicht fällt Ihnen selber eine solche Aufgabe ein. Oder Sie kennen eine solche Aufgabe aus dem Schulkurs Informatik. Oder Sie haben eine interessante Aufgabe im Internet gefunden. Wie auch immer: Informieren Sie uns, helfen Sie uns dabei, mehr Aufgaben ohne Mathe zu finden. Danke!

Davon abgesehen: Coole Ideen für mehr Aufgaben mit(!) Mathe sind uns natürlich auch willkommen. Jeder soll seinen Spaß haben!   
****

Sie stellen fest, dass wir bislang keine echten Aufgaben zu verschachtelten, inneren und anonymen Klassen haben. Das ist vorrangig dem Umstand geschuldet, dass solche Aufgaben oft ein wenig gekünstelt daherkommen; vielleicht sind wir auch nur etwas phantasielos. Aber oft kann man das gleiche Programm auch ohne verschachtelte, innere oder anonyme Klassen schreiben. Beherrschen und können müssen Sie diese Konstrukte natürlich trotzdem -- mindestens müssen Sie fremden Code lesen und verstehen können, der davon Gebrauch macht.

== Flugzeuge

Schreiben Sie eine Klasse `Airplane`, die folgende Member besitzt: eine Zeichenkette `name`, einen Modellnamen `modell` und eine maximale Geschwindigkeit `maxSpeed` in mph.

Legen Sie zwei Konstruktoren an: einen Default-Konstruktor, der sinnvolle Werte für ein Flugzeug wählt, und einen Konstruktor, der alle Member der Klasse mit Werten belegt.

Schreiben Sie nun die Methode `fly`; sie bekommt eine Geschwindigkeit übergeben und liefert nichts zurück. Innerhalb der Methode wird geprüft, ob die zulässige maximale Geschwindigkeit nicht überschritten wird. Wenn das korrekt ist, wird z.B. der Text "Flugzeug Obama fliegt 1000 mph." ausgegeben. Ist die zulässige Geschwindigkeit überschritten, kommt etwas wie: "Zu schnell! Flugzeug Olpe darf nur maximal 900 mph fliegen.".
Probieren Sie Ihr Programm aus, indem Sie ein paar Flugzeuge anlegen und diese fliegen lassen.

Erweiterung:
Sie möchten nun Ihr Programm erweitern und eine ganze Reihe an Flugzeugen fliegen lassen. Dazu schreiben Sie eine weitere Methode `flyAll`. Sie gibt nichts zurück und erwartet als Parameter ein Array mit Flugzeugen und eine Geschwindigkeit. Innerhalb der Methode soll jedes Flugzeug aus dem Array mit der angegebenen Geschwindigkeit fliegen. Testen Sie nun auch diese Methode und legen dazu vier Flugzeuge an.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Airplane.java[]
include::{sourcedir}/useAirplane.java[]
----
include::postDetails.adoc[]

== Ratespiel

Sie denken an eine Zahl von 1 bis 10 (die Obergrenze kann man frei wählen) und Ihr Programm versucht die Zahl zu erraten in so wenig Versuchen wie möglich. So sieht der Dialog über die JShell aus:

----
jshell> Quizz q = new Quizz(10)
q ==> Quizz@11758f2a

jshell> q.ask()
$89 ==> "Is the number <= 5?"

jshell> q.answer(false)
$90 ==> "Is the number <= 8?"

jshell> q.answer(true)
$91 ==> "Is the number <= 7?"

jshell> q.answer(false)
$92 ==> "The number is 8"
----

Das Programm stellt immer nur die Frage, ob die gesuchte Zahl kleiner oder gleich einer Obergrenze ist -- solange, bis die Zahl gefunden ist.

Das Rateprogramm besteht aus der Klasse `Quizz`, einem Konstruktor, dem die größte zu erratende Zahl mitgegeben wird, und den zwei öffentlichen Methoden `ask` und `answer`. Leiten Sie die Methodenköpfe aus dem gezeigten Dialog ab. Wenn hilfreich, können Sie private Methoden ergänzen.

TIP: Ein kleiner Tipp: Die Methode `answer` nutzt die Methode `ask`.

Zeigen Sie anhand zweier automatisierter Ratedurchläufe, dass Ihr Programm korrekt arbeitet.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}\guessNumber.java[]
----

.Beispielhafte Tests
[source,java]
----
include::{sourcedir}\guessNumber.test.java[]
----
include::postDetails.adoc[]

== Klassendefinition

Schreiben Sie die Syntax einer Klassendefinition auf, indem Sie die folgenden Begriffe in die richtige Reihenfolge (falls diese relevant ist) bringen und durch die Zeichen ergänzen, die zwischen den jeweiligen Teilen stehen müssen. Geben Sie für jeden der Begriffe ein Beispiel an, so dass aus ihrer Syntaxdefinition eine korrekte Java-Klassendefinition wird.

* Feld
* Methode(statisch)
* Methode(nicht-statisch)
* `class`
* Klassenname
* Konstruktor
* `static`

**Hinweis**: Die Aufgabe kann man noch besser formulieren. Das merken Sie, wenn Sie die Lösung gesehen haben. Haben Sie einen Vorschlag?

include::preDetailsSolution.adoc[]
----
class Klassenname {
  Feld
  static Methode(statisch)
  Methode(nicht-statisch)
  Konstruktor
}

// Beispiel:
class MyClass {
  int myField;
  static void myStaticMethod() { }
  void myMethod() { }
  MyClass() { }
}
----
include::postDetails.adoc[]

== Auto

Das Datenmodell eines Autos sei als Klasse `Car` modelliert. Folgende Methoden gibt es:

* Die Methode `setZero` setzt den Tachostand auf Null.
* Die Methode `drive` verändert den Tachostand unter Angabe einer durchschnittlichen Geschwindigkeit und der verbrauchten Zeit.
* Die Methode `mileage` (Spritverbrauch) rechnet anhand der gefahrenen Kilometer und des verbrauchten Kraftstoffs den Kraftstoffverbrauch pro 100km aus.

Programmieren Sie die Klasse `Car`, ergänzen Sie geeignete Felder und entscheiden Sie, welche Methoden statisch und welche nicht statisch umzusetzen sind.

Reproduzieren Sie den beispielhaft gezeigten Dialog mit der JShell: Sie setzen den Tachostand auf Null, legen eine Strecke zurück und berechnen anschließend -- beim Tanken -- den Spritverbrauch.

----
include::{sourcedir}/Auto.jshell.txt[]
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Auto.java[]
----
include::postDetails.adoc[]

== Bruchrechner

Mit der Klasse `Fraction` (Bruch) bauen Sie sich einen "Taschenrechner" zum Rechnen mit Brüchen. Die zwei Argumente des Konstruktoraufrufs entsprechen dem Zähler (_numerator_) und dem Nenner (_denominator_) eines Bruchs. Der Konstruktor, mit einem ganzzahligen Argument aufgerufen, entspricht einem Bruch mit einem Nenner von 1; so entspricht beispielsweise die Zahl 3 dem Bruch 3/1.

Es gibt die folgenden Rechenmethoden; entscheiden Sie selbst, welche Methoden sinnvoll als statisch anzulegen sind:

* `long gcd(long a, long b)` berechnet den größten gemeinsamen Teiler (_greatest common divisor_) zweier ganzer Zahlen. Nutzen Sie den in der https://en.wikipedia.org/wiki/Greatest_common_divisor[englischen Wikipedia] beschriebenen, rekursiven Algorithmus.
* `long lcm(long a, long b)` berechnet das kleinste gemeinsame Vielfache (_least common multiple_) zweier ganzer Zahlen. Auch hier verrät Ihnen die https://en.wikipedia.org/wiki/Least_common_multiple[englische Wikipedia], wie sich `lcm` mittels `gcd` berechnen lässt.
* `Fraction neg()` liefert den negierten Bruch; die Negation von 2/3 ist -2/3.
* `Fraction inv()` liefert den inversen Bruch; die Inversion von 2/3 ist 3/2.
* `Fraction add(Fraction f)` berechnet den Ergebnisbruch, der sich ergibt, wenn man den aktuellen Bruch mit dem Bruch `f` addiert.
* Die Methoden `sub`, `mul`, `div` entsprechen im Aufbau `add` und führen die Subtraktion, die Multiplikation und Division durch. Die Implementierungen von `sub` und `div` sollen auf die Implementierungen von `add` bzw. `mul` zurückgreifen und dazu `neg` bzw. `inv` verwenden.

Die `public String toString()`-Methode ist ebenfalls zu implementieren -- beachten Sie das vorangestellte (und an dieser Stelle notwendige) `public`. Die Methode liefert eine Repräsentation eines Bruchs als Zeichenkette zurück. Folgendes gilt:

* Brüche werden intern stets gekürzt, so dass der Bruch 4/12 als `1/3` dargestellt wird.
* Brüche, die gekürzt eine Eins im Nenner haben (sogenannte "Scheinbrüche"), werden als Ganzzahlen dargestellt: aus 3/1 wird `3` und aus -50/5 (als Folge des Kürzens) eine `-10`.
* Bei "unechten Brüchen" ist der Zähler größer als der Nenner. Dann wird der Bruch als gemischter Bruch dargestellt mit seinem ganzzahligen Anteil: aus 7/3 wird `2 1/3`; beachten Sie das Leerzeichen.
* Bei einem negativen Bruch wird das Vorzeichen (das Minuszeichen) der Darstellung des positivwertigen Bruchs vorangestellt: es heißt z.B. `-2 1/3` oder `-5/4` niemals jedoch `2 -1/3` oder `5/-4`.

Ihr Code muss die folgenden Testfälle fehlerfrei durchlaufen. Speichern Sie die Tests z.B. in einer Datei namens `fraction.test.java`. Starten Sie den Testlauf mit dem `/open`-Befehl der JShell.

.Tests zur Aufgabe
[source,java]
----
include::{sourcedir}/fraction.test.java[]
----

include::preDetailsSolution.adoc[]
In der Lösung sind die Felder der Klasse zum einen `private`, um Zugriffe von außen zu unterbinden, und `final`, um die Unveränderlichkeit der Werte eines Bruchs zu erzwingen. Denn keine der Methoden wie z.B. `add` soll die einmal gesetzten Werte von Zähler oder Nenner eines Bruchs verändern können. Stattdessen wird ein neuer Bruch als Ergebnis einer Rechnung erzeugt.

[source,java]
----
include::{sourcedir}/fraction.java[]
----
include::postDetails.adoc[]

== Geometrie

Ein typischer Grund eine eigene Klasse zu bauen ist, dass man mehrere Variablen zu einer gedanklichen Einheit zusammenfügen möchte. Ein Paradebeispiel dafür ist die Arbeit mit geometrischen Figuren. Angefangen beim einfachsten Beispiel: dem Punkt.

=== Punkte

Ein Punkt im zweidimensionalen Raum besteht aus einer x- und einer y-Koordinate. Schreiben Sie eine Klasse `Point`, die einen Punkt in einem digitalen Bild darstellt. Wählen Sie dazu einen geeigneten Datentyp für die benötigten Felder und geben Sie ihrer Klasse einen sinnvollen Konstruktor.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Point.java[]
----
<1> Der Typ `int` bietet sich an, weil man in digitalen Bilern in der Regel nicht mit Bruchteilen von Pixeln rechnet und weil kleinere Datentypen (`short` oder `byte`) im weiteren Code Typkonvertierungen nötig machen würden, die die Lesbarkeit des Codes verringern. Wenn man besonders viele Elemente vom Typ `Point` speichern möchte, kann aber der Typ `short` durchaus Sinn machen.
<2> Der Konstruktor sollte sowohl x- als auch y-Koordinate übernehmen. Einen Punkt mit nur einer Koordinate zu definieren macht wenig Sinn.
include::postDetails.adoc[]

=== Distanz

Fügen Sie ihrer Klasse `Point` eine Methode `dist` hinzu, die die Distanz zwischen zwei Punkten errechnet.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Point_dist.java[]
----
include::postDetails.adoc[]

=== Punkt in Polarkoordinaten

Wenn man mit beweglichen Objekten umgehen muss, ist es manchmal einfacher einen Punkt nicht in karthesischen Koordinaten (x- und y-Koordinate) auszudrücken sondern in Polarkoordinaten (Richtung und Distanz).

Schreiben Sie eine Klasse `PolarPoint`, die einen Punkt in Polarkoordinaten darstellt. Der Punkt ist definiert über die Distanz zum Ursprung und den Winkel (im Bogenmaß) zur x-Achse (gegen den Uhrzeigersinn).

Implementieren Sie nun eine Methode `toPolar` in der Klasse `Point` und eine Methode `toCartesian` in der Klasse `PolarPoint`, die die Umrechung nach der folgenden mathematischen Definition erlaubt:

----
x = cos(alpha) * dist
y = sin(alpha) * dist

alpha = atan2(y, x)         <1>
dist = sqrt(x^2 + y^2)
----
<1> Die mathematische Funktion atan2 ist eine Variante des Arkustangens, der Umkehrfunktion des Tangens, mit zwei Parametern. Sie erlaubt es, den korrekten Winkel zu finden egal in welchem Quadranten dieser sich befindet. In Java existiert sie als `Math.atan2`.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/PolarPoint.java[]
----
<1> Je weiter ein Punkt entfernt vom Ursprung liegt, desto höher muss der Winkel in Polarkoordinaten aufgelöst sein, um Rechenfehler gering zu halten. Daher ist `double` hier die sinnvollste Wahl für den Datentyp. Wenn Speicherplatz besonders wichtig wird, könnte man auch `float` verwenden.
include::postDetails.adoc[]

=== Linien

Schreiben Sie eine Klasse `Line`, die eine Linie darstellt. Eine Linie besteht aus zwei Punkten.

Implementieren Sie in dieser Klasse eine Methode `slope`, die die Steigung der Linie bestimmt.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Line.java[]
----
include::postDetails.adoc[]

=== Dreiecke

Schreiben Sie eine Klasse `Triangle`, die ein Dreieck darstellt. Ein Dreieck besteht aus drei Punkten.

Implementieren Sie in dieser Klasse eine Methode `getSides`, die ein Array mit den drei Linien zurückgibt, die zwischen den Punkten bestehen.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Triangle.java[]
----
include::postDetails.adoc[]

=== Zeichnen auf der Konsole

Erstellen Sie ein zweidimensionales Array vom Typ `char`. Dieses Array soll ein Bild mit einer Breite von 20 und einer Höhe von 10 Pixeln darstellen.

Schreiben Sie eine Methode `printImage`, die das Array als "Bild" auf der Konsole ausgibt.

Schreiben Sie eine Methode `drawLine`, die eine gerade Linie in dieses Bild zeichnet.

CAUTION: Es ist gar nicht so einfach, einen sinnvollen Algorithmus zum Zeichnen einer Linie zu finden. Sie dürfen sich dazu auch gerne vorher im Internet schlau machen.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/drawLine.java[]
----
include::postDetails.adoc[]

== Stapel

Ein Stapel (engl. _stack_) ist eine Datenstruktur, in der Elemente aufeinander abgelegt (eben "gestapelt") werden können, so dass immer nur das oberste Element sichtbar ist.

Java stellt eine Stapel-Implementierung unter dem Klassennamen `Stack` bereit. In dieser Aufgabe sollen Sie sich selbst an einer Umsetzung versuchen. Um Konflikten mit der Java-Klasse aus dem Weg zu gehen, nennen wir unsere Klasse `Stakk`. Die Klasse `Stakk` soll einen Stapel mit Werten vom Typ `int` implementieren. Die Klasse `Stakk` soll zwei Felder haben:

* ein Feld vom Typ `int`, das dem aktuell obersten Wert des Stapels entspricht
* ein Feld vom Typ `Stakk`, das dem Rest des Stapels entspricht; ein leerer Stapel wird mit einem "Rest" von `null` ausgewiesen, das andere Feld ist in dem Fall irrelevant

=== Erstellen Sie den Konstruktor

Programmieren Sie die Konstrukturen der Klasse `Stakk` und die Methode `toString()` zur Ausgabe des Stapels auf der Konsole. Folgende Interaktionen über die JShell müssen möglich sein:

----
jshell> new Stakk()
$3 ==> -|

jshell> new Stakk(1)
$4 ==> 1, -|

jshell> new Stakk(2, new Stakk(1, null))
$5 ==> 2, -|

jshell> new Stakk(2, new Stakk(1, new Stakk()))
$6 ==> 2, 1, -|
----


Das Symbol `-|` bei der Ausgabe zeigt das untere Ende des Stapels an. Die Werte links davon liegen nacheinander "über" dem Ende. Im letzten Beispiel ist `2` der oberste Wert auf dem Stapel.

[WARNING]
====
Die Methode `toString` muss den Sichtbarkeitsmodifizierer `public` bekommen, weil es eine Methode mit diesem Namen und dieser Sichtbarkeit bereits in der Klasse `Objekt` gibt, von der alle Java-Klassen erben. Überschreibt man diese Methode, darf man ihre Sichtbarkeit dabei nicht einschränken.
====

include::preDetailsSolution.adoc[]
[source,java]
----
class Stakk {
  int value;
  Stakk rest;

  Stakk(int value, Stakk rest) {
    this.value = value;
    this.rest = rest;
  }

  Stakk() { this(0, null); }

  Stakk(int value) { this(value, new Stakk()); }

  public String toString() {
    if (rest == null) return "-|";
    return value + ", " + rest.toString();
  }
}
----
include::postDetails.adoc[]

=== Größe ermitteln

Fügen Sie eine Methode `size` zu Ihrer Klasse hinzu, die die Größe des Stapels ermittelt.

----
jshell> Stakk deck = new Stakk(10, new Stakk(11, new Stakk(5)))
deck ==> 10, 11, 5, -|

jshell> deck.size()
$8 ==> 3
----

[TIP]
====
Die Klasse `Stakk` stellt eine rekursive Datenstruktur dar: Ein Stakk besteht zum Teil aus einem weiteren Stakk. Sie werden feststellen, dass es einfacher ist, Methoden, die mit solchen rekursiven Strukturen umgehen müssen, ebenfalls rekursiv zu definieren.
====

=== Hinzufügen

Fügen Sie eine Methode `push` vom Rückgabetyp `void` zu Ihrer Klasse hinzu, die ein Element zum Stapel hinzufügt.

----
jshell> deck.push(7)

jshell> deck
deck ==> 7, 10, 11, 5, -|
----

=== Entfernen

Fügen Sie eine Methode `pop` zu Ihrer Klasse hinzu, die das oberste Element vom Stapel entfernt und dieses Element zurückgibt.

----
jshell> deck.pop()
$11 ==> 7

jshell> deck
deck ==> 10, 11, 5, -|
----

=== Umwandlung in Array

Fügen Sie eine Methode `toArray` zu Ihrer Klasse hinzu, die den gesamten Stapelinhalt als Array zurückgibt. Lösen Sie die Aufgabe mit einer `for`-Schleife.

----
jshell> deck.toArray()
$13 ==> int[3] { 10, 11, 5 }
----

=== Erzeugen aus Array

Fügen Sie eine statische Methode `fromArray` zu Ihrer Klasse hinzu, die aus einem Array einen Stapel erzeugt.

----
jshell> Stakk s = Stakk.fromArray(new int[]{1,2,3,4,5})
s ==> 1, 2, 3, 4, 5, -|
----

=== Mischen (mit Umwandlung)

Schreiben Sie eine Methode `shuffle`, die eine zufällig durchmischte Version ihres Stapels zurückgibt. Verwenden Sie zur Implementierung die Methoden `toArray` und `fromArray`, um den Stapel erst in ein Array umzuwandeln, das Array zu mischen und dann einen neuen Stapel aus dem gemischten Array zu erzeugen.

----
jshell> s.shuffle()
$16 ==> 5, 1, 2, 4, 3, -|

jshell> s.shuffle()
$17 ==> 3, 5, 2, 4, 1, -|
----

=== Lösung

Hier findet sich der Code, der alle vorherigen Aufgabenteile abdeckt.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Stack_full.java[]
----

Tests zum Code:

[source,java]
----
include::{sourcedir}/Stack_full.Test.java[]
----
include::postDetails.adoc[]

=== Black Jack

Benutzen Sie ihre Stack-Implementierung um das Spiel "Black Jack" für einen einzelnen Spieler zu implementieren -- bzw. die etwas einfachere Variante "17 + 4". Dabei verwenden wir in unserer Variante nur einen einzigen Kartensatz mit 32 Karten. Der Spieler kann in jeder Runde entscheiden, ob er noch eine weitere Karte haben möchte und muss versuchen, mit der Summe der Kartenwerte so nah wie möglich an die 21 (den "Black Jack") heranzukommen.

Schreiben Sie zunächst eine Methode `createDeck`, die einen (sortierten) Stapel mit 32 Spielkarten erzeugt. Mischen Sie diesen Stapel dann mit ihrer Methode `shuffle`. So lange noch ein Zug möglich ist, muss nun der Nutzer nach seiner Entscheidung gefragt werden, um dann die neue Summe der Kartenzahlen angezeigt zu bekommen. Um das Spiel etwas interessanter zu machen können Sie auch noch gegnerische Spieler simulieren.

[TIP]
====
Normalerweise würde man die Interaktion zur Abfrage einer Nutzerentscheidung mit der Java-Klasse `Scanner` realisieren. Sie können aber auch sehr einfach ein kleines Fenster mit der Abfrage öffnen, indem Sie die Klasse `javax.swing.JOptionPane` importieren und mit ihrer Hilfe ein Dialogfenster öffnen. Während der Abfrage wartet die JShell auf Antwort durch das Dialogfenster.

----
jshell> import javax.swing.JOptionPane;
jshell> JOptionPane.showConfirmDialog(null, "Nächste Karte?")
----
====

////
Die hier auskommentierte Lösung ist nicht mehr gültig!

include::preDetailsSolution.adoc[]
.Lösung
[source,java]
----
include::{sourcedir}/blackJack.java[]
----
include::postDetails.adoc[]
////

== Tiere

Schreiben Sie eine Klasse `Animal`, die Membervariable für folgende Eigenschaften besitzt: Name `name`, Tierart `species`, Tierlaut `sound` und Alter `age`. Wählen Sie die passenden Datentypen und begründen Sie, warum Sie sie gewählt haben. Nutzen sie das Schlüsselwort `private`.

Schreiben Sie zwei Konstruktoren: den Default-Konstruktor und einen weiteren, der alle Member des Objektes durch Parameter belegt.

Desweiteren gibt es Methoden (die Methodensignaturen sind nicht vollständig):

* getAge -- gibt das Alter zurück
* makeSound -- gibt den Ton des Tieres so oft zurück, wie man ihn mit `quantity` festlegt.
* getIdentity -- liefert Name und Art in einem Satz kombiniert zurück.
* countAllAges -- gibt die Summe der Alter aller Tiere, die der Methode in einem Array übergeben werden zurück.

Schreiben Sie auch Code, um Ihre Klasse zu testen. Die Ausgabe könnte z.B. so sein:

----
Ich heiße Alma und bin ein/eine Kuh.
Ich heiße Elsa und bin ein/eine Kuh.
Ich heiße Lulu und bin ein/eine Schildkröte.
Ich heiße Dori und bin ein/eine Fisch.
muh

muhmuh

klapklapklap

blubbblubbblubbblubb

Zusammen sind wir 120 Jahre alt.
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/Animal.java[]
include::{sourcedir}/useAnimal.java[]
----
include::postDetails.adoc[]

== Partially Accurate Battle Simulator

In dieser Aufgabe sollen Sie versuchen die Logik einer vereinfachten Version des Spiels https://www.youtube.com/watch?v=OTV4Yfi6VwM[Totally Accurate Battle Simulator] zu implementieren.

In Unserer Version des Spiels wird die Klasse `Unit` eine Einheit von beliebig vielen Kämpfern des gleichen Typs darstellen. Diese Klasse hat die folgenden Eigenschaften (Felder):

* Anzahl der Einheiten
* Typ der Einheit
* Hitpoints einer einzelnen Einheit
* Schaden einer einzelnen Einheit

Das Spiel hat die folgenden Einheitentypen:

* Pikeniere (_pikemen_)
* Boxer (_boxer_)
* Katapulte (_catapults_)

Der Spielablauf wird in einer Methode `simulateBattle` simuliert, die zwei Arrays vom Typ `Unit` übernimmt und den Kampf zwischen diesen zwei Armeen rundenbasiert simuliert. In jeder Runde trifft eine zufällig ausgewählte Einheiten aus dem ersten Array auf eine aus dem zweiten. Der Schaden beider Einheiten wird mit der jeweiligen Anzahl der Kämpfer multipliziert und durch die Hitpoints der gegnerischen Einheit geteilt, um zu bestimmen wie viele gegnerische Kämpfer getötet wurden. Wenn noch Kämpfer in einer Einheit übrig bleiben, kann sie in der nächsten Runde noch einmal ausgewählt werden. Es werden so viele Runden gespielt, wie nötig sind, bis eine der beiden Seiten keine Einheiten mehr übrig hat.

Die Ausgabe der Simulation soll in etwa wie folgt aussen:
----
jshell> simulateBattle(blue, red);
Blau:
   17  Pikeniere
    4      Boxer
Rot:
    1  Katapulte
   10      Boxer
17 blaue Pikeniere gegen 10 rote Boxer:
   10  rote      Boxer sterben
   10 blaue  Pikeniere sterben
4 blaue Boxer gegen 1 rote Katapulte:
    1  rote  Katapulte sterben
    4 blaue      Boxer sterben
Blau gewinnt und behält:
    7  Pikeniere
----

=== Simulation

Schreiben Sie die Klasse `Unit` und die Methode `simulateBattle`.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/simulateBattle.java[]
----
include::postDetails.adoc[]

=== Einheitentypen

Damit die Einheitentypen sich nicht nur in ihrer Stärke unterscheiden, fügen wir zu unserem Spiel die folgenden Regeln hinzu:

* Pikeniere richten doppelten Schaden bei Boxern an.
* Der Schaden von Katapulten gegen Boxer und Pikeniere ist verdreifacht.
* Katapulte nehmen nur den halben Schaden von Boxern.

Fügen Sie der Klasse `Unit` eine Methode `multiplierAgainst` hinzu, die errechnet welchen Schadensmultiplikator die Einheit im Kampf gegen eine andere (als Argument übergebene) Einheit erhält und berücksichtigen Sie diesen Multiplikator in `simulateBattle`.

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/simulateBattle_multiplier.java[]
----
include::postDetails.adoc[]